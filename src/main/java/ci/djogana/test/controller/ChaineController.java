package ci.djogana.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.djogana.test.business.ChaineBusiness;
import ci.djogana.test.dto.ChaineDto;

@RestController
@RequestMapping(value="/chaine")
public class ChaineController {

	@Autowired
	private ChaineBusiness chaineBusiness;
	
	@RequestMapping(value="/infosChaine",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public ChaineDto getInfosChaine(@RequestBody ChaineDto dto) {
		
		return chaineBusiness.calculVoyellesEtConsonnes(dto);
	}
}
