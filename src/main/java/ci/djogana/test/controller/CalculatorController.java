package ci.djogana.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.djogana.test.business.CalculatorBusiness;
import ci.djogana.test.dto.CalculatorDto;

@RestController
@RequestMapping(value="/calculatrice")
public class CalculatorController
{
	@Autowired
	private CalculatorBusiness calculatorBusiness;

	@RequestMapping(value="/calcul",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Double getCalculator(@RequestBody CalculatorDto dto) {
    	return calculatorBusiness.calculatrice(dto);
    }
}
