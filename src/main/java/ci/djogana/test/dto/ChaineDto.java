package ci.djogana.test.dto;

public class ChaineDto {

	private String chaine;
	private Integer nombreVoyelles;
	private Integer nombreConsonnes;
	
	public ChaineDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ChaineDto(String chaine, Integer nombreVoyelles, Integer nombreConsonnes) {
		super();
		this.chaine = chaine;
		this.nombreVoyelles = nombreVoyelles;
		this.nombreConsonnes = nombreConsonnes;
	}
	
	public String getChaine() {
		return chaine;
	}
	public void setChaine(String chaine) {
		this.chaine = chaine;
	}
	public Integer getNombreVoyelles() {
		return nombreVoyelles;
	}
	public void setNombreVoyelles(Integer nombreVoyelles) {
		this.nombreVoyelles = nombreVoyelles;
	}
	public Integer getNombreConsonnes() {
		return nombreConsonnes;
	}
	public void setNombreConsonnes(Integer nombreConsonnes) {
		this.nombreConsonnes = nombreConsonnes;
	}
	
	
}
