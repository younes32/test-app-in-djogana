package ci.djogana.test.dto;

public class CalculatorDto 
{
  private Double operande1;
  private Double operande2;
  private Double resultat;
  private String operateur;
  
public CalculatorDto() {
	super();
	// TODO Auto-generated constructor stub
}
public Double getOperande1() {
	return operande1;
}
public void setOperande1(Double operande1) {
	this.operande1 = operande1;
}
public Double getOperande2() {
	return operande2;
}
public void setOperande2(Double operande2) {
	this.operande2 = operande2;
}
public Double getResultat() {
	return resultat;
}
public void setResultat(Double resultat) {
	this.resultat = resultat;
}
public String getOperateur() {
	return operateur;
}
public void setOperateur(String operateur) {
	this.operateur = operateur;
}
  

}
