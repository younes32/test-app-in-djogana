package ci.djogana.test.business;

import java.util.Arrays;

import org.springframework.stereotype.Component;

import ci.djogana.test.dto.ChaineDto;

@Component
public class ChaineBusiness {

	public ChaineDto calculVoyellesEtConsonnes(ChaineDto dto) {
		
		int nombreDeVoyelles = 0;
		int nombreDeConsonnes = 0;
		try {
			if(dto.getChaine() == null) {
				throw new Exception("veuillez saisir une chaine");
			}
			char[] chaineTemp = dto.getChaine().toCharArray();
			
			for(char c : chaineTemp) {
				if(Arrays.asList("a","e","i","o","u","y").contains(String.valueOf(c).toLowerCase())) {
					nombreDeVoyelles ++;
				}else {
					nombreDeConsonnes++;
				}
//				if(c == 'a' || c == 'e' || c == 'i'|| c == 'o'|| c == 'u'|| c == 'y' || c == 'A' || c == 'E' || c == 'I'|| c == 'O'|| c == 'U'|| c == 'Y') {
//					nombreDeVoyelles ++;
//				}else {
//					nombreDeConsonnes++;
//				}
				
			}
			dto.setNombreVoyelles(Integer.valueOf(nombreDeVoyelles));
			dto.setNombreConsonnes(Integer.valueOf(nombreDeConsonnes));
		}catch(Exception e) {
			
		}
		return dto;
	}
	public ChaineDto calculNbrCharter(ChaineDto dto, char carac) {
		int nbreApparition = 0;
		try {
			if(dto.getChaine() == null) {
				throw new Exception("veuillez saisir une chaine");
			}
			char[] chaineTemp = dto.getChaine().toCharArray();
			
			for(char c : chaineTemp) {
				if( c) {
					nombreDeVoyelles ++;
				}else {
					nombreDeConsonnes++;
				}
		}
		}
		dto.setNombreVoyelles(Integer.valueOf(nombreDeVoyelles));
		dto.setNombreConsonnes(Integer.valueOf(nombreDeConsonnes));
	}catch(Exception e) {
		
	}
		return ;
	
}
