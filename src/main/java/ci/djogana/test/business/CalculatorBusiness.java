package ci.djogana.test.business;

import org.springframework.stereotype.Component;

import ci.djogana.test.dto.CalculatorDto;
@Component
public class CalculatorBusiness 
{
   public Double calculatrice(CalculatorDto dto) {
	   
	   Double resultat = null;
	   switch(dto.getOperateur()) {
	   
	   case "+":
		   resultat = dto.getOperande1()+dto.getOperande2();
		   break;
	   case "-":
		   resultat = dto.getOperande1()-dto.getOperande2();
		   break;
	   case "*":
		    resultat = dto.getOperande1()*dto.getOperande2();
		   break;
	   case "/":
		   resultat = dto.getOperande1()/dto.getOperande2();
		   break;
	   case "%":
		   resultat = dto.getOperande1()%dto.getOperande2();
		   break;
		   }
	   dto.setResultat(resultat);
	   return dto.getResultat();
   }
}
